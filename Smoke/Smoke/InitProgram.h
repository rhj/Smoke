/*
	InitProgram.h
	ver:0.1.0
	初始化的头文件
	包含一些初始化函数的声明
*/
#ifndef INITPROGRAM_H_
#define	INITPROGRAM_H_





/*————————头文件包含————————*/

#include <graphics.h>
#include "Smoke.h"

/*————————程序预处理————————*/

#define WIN_X 800
#define WIN_Y 600



/*————————函数的声明————————*/

void InitGame();					//初始化游戏
void InitGraphics(void);			//图形库的初始化
void InitChapter1();				//初始化第一关
void InitChapter2();				//初始化第二关
void InitExitValue();				//退出关卡初始化数据

//void InitVolue(void);				//初始化全局变量




#endif // !INITPROGRAM_H_