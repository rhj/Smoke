/*
	Smoke.cpp
	ver:0.1.0
	程序的主文件，main函数的存放位置
*/

/*————————头文件包含————————*/

#include "Smoke.h"

/*————————函数的定义————————*/

int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	//	判断程序是否重复运行
	HWND hwnd = GetHWnd();

	HANDLE m_hMutex = CreateMutex(NULL, TRUE, L"cplusplus_me");
	DWORD dwRet = GetLastError();
	if (m_hMutex)
	{
		if (ERROR_ALREADY_EXISTS == dwRet)
		{
			MessageBox(hwnd, L"游戏已经在运行！", L"警告", MB_OK);
			CloseHandle(m_hMutex);
			exit(1);
		}
	}
	else
	{
		MessageBox(hwnd, L"游戏已经在运行！", L"警告", MB_OK);
		exit(1);
	}
	InitGraphics();		//初始化图形界面
//	ShowStart();		//显示开始界面
	MainMenu();

	
}
