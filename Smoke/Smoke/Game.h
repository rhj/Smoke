/*
	Game.h
	ver:0.1.0
	游戏主体框架
	存放一些游戏主体的函数头文件
*/

#ifndef GAME_H_
#define	GAME_H_
/*————————头文件包含————————*/
#include "Graph.h"
#include <math.h>
#include <time.h>
#include <stdio.h>

/*————————程序预处理————————*/

#define PI 3.1415926
#define	STEP	4
#define NSTEP	2			//当图片未移动时的步长
#define GAMEOVER	0		//游戏失败
#define NEXTCP		1		//进入下一关

extern bool *g_bkhaveimg;		//背景数据指针
extern bool *g_bkhinder;		//障碍物位置指针
extern bool *g_bkhvimgbk;					//背景数据指针备份
extern int g_x, g_y;			//PM2.5所在位置坐标
extern int g_PM_x[11];			//PM2.5尖端所在坐标
extern int g_PM_y[11];			//PM2.5尖端所在坐标
extern wchar_t *g_Chapter2BK;
extern wchar_t *g_Chapter2BW;
extern int g_CreateHinderHighRandNum;	//随机生成障碍物中数值范围
extern int g_CreateHinderHigh;			//生成障碍物固定距离

/*————————函数的声明————————*/
void StartGame();					//开始游戏
int Chapter1(void);				//第一关
int Chapter2();					//第二关
//void Chapter1Test();
bool CheckHinder();					//检测是否碰到障碍物

#endif // !GAME_H_
