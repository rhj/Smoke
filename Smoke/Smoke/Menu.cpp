/*
	Menu.cpp
	ver:0.1.0
	菜单源文件
	包含一些与菜单相关的函数定义
*/

/*————————头文件包含————————*/

#include "Menu.h"
#include "Graph.h"

/*————————程序预处理————————*/

int g_menu_y[4] = { 215, 285, 355, 425 };		//菜单项左上角y坐标
int g_menu_x = 320;								//菜单项左上角x坐标
int g_menu_y_size = 35;							//菜单项右下角相对左上角y坐标增量
int g_menu_x_size = 160;						//菜单项右下角相对左上角x坐标增量

int g_menu = 0;									//当前菜单所在位置
bool g_enter_menu = false;						//进入当前所在菜单

/*————————函数的定义————————*/
/*
	开始游戏：215，320，250，480
	游戏设置：285，320，320，480
	帮助说明：355，320，390，480
	退出游戏：425，320，460，480
*/
void MainMenu(void)
{
	//	显示菜单背景
	ShowMenuBK();
	
	//	显示选项框
	ShowSelect(g_menu);		//在第一个菜单上显示选项框
	int _menu = g_menu;
	while (1)
	{
		KeyControl();			//统计按键状态，计算菜单应在的地方
		MouseControl();			//统计鼠标状态
		if (_menu != g_menu)
		{
			_menu = g_menu;
			loadimage(NULL, _T("JPG"), _T("MENUBACK"));		//	重新加载主菜单图片，清除上一状态
			ShowSelect(g_menu);
		//	Sleep(150);
		}
		if (g_enter_menu)	//进入相应功能选单
		{
			Sleep(150);
			MenuSelect(g_menu);
			
			setlinecolor(YELLOW);
			loadimage(NULL, _T("JPG"), _T("MENUBACK"));
			ShowSelect(g_menu);
			Sleep(150);
		}
	}
}

//	进入相应菜单
void MenuSelect(int n)
{
	//	清除原状态
	g_enter_menu = false;
	switch (n)
	{
	case 0:			//开始游戏
		StartGame();
		break;
	case 1:			//游戏设置

		break;
	case 2:			//帮助说明
	//	Chapter2();
		break;
	case 3:			//退出游戏
		ExitGame();
		break;
	}
	InitExitValue();	//删除动态分配的空间
	g_menu = 0;
}

//	退出游戏的函数
void ExitGame(void)
{
	//	若有时间重写此函数不要使用WINAPI
	HWND hwnd = GetHWnd();
	if (MessageBox(hwnd, _T("是否退出游戏？"), _T("提醒"), MB_YESNO) == IDYES)
		exit(0);
}